# docker/wildfly_postgres

Docker Compose to install Wildfly Java EE application server and PostgreSQL database

Before using the docker.compose.yml, define the following environment variables:

* APP_DIR is the directory where the Java EE application is located
* POSTGRES_USER is the database user
* POSTGRES_PASSWORD is the password of the database user
* POSTGRES_DB is the database name
* WEB_PORT is the host port to which the application server is mapped


Two services are created:

* app-server: WildFly application server
* database: PostgreSQL database

